# 📕 Libft
[![hmechich's 42Project Score](https://badge42.herokuapp.com/api/project/hmechich/Libft)](https://github.com/JaeSeoKim/badge42)  
**Do not copy this code and think harder**  
### This project aims to code a *C library* regrouping usual functions that you’ll be use for all your next projects.
### Subject Version: `15`
